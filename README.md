
#Reliable Proxy


##Summary
      

ReliableProxy (RP) is a mediator that sits between client and server which aims to handle the following types of failure:

 - Server crashes
 - Server restarts
 - Temporary network failure.

##Architecture


ReliableProxy has been implemented to be fault tolerance to a set of common failures (see below) and following the non-blocking (EDA) principles. ::

Client (socketA) <--> (socketB) ReliableProxy (socketC) <---> (socketD) Server

##Failure handle

 - if socketA is closed ==> RP will be restarted
 - if socketB fails ==> RP will be restarted
 - if new client is detected (socketA2) ==> RP will close socketA and will accept socketA2
 - if socketC cannot connect to Server ==> RP will try to reconnect. If after N attempts (see Configuration), connection cannot be established, the system will be gracefully shutdown.
 
 RP is said to be in failure_mode if the client side (socketA-socketB) is established but not the backend side (socketC-socketD). While in failure_mode all the incoming requests from client will be buffered internally in the RP, and flushed to server as soon as backend side connection is established. Any other unexpected error captured by the RP will stop (not shutdown) system.

 ## Configuration
 
		 akka {
		    ...
			proxy {
					// RP listening port
					listening-tcp-port  = 7878
					// Details for the target server 
					target-host = "localhost"  
					target-port = 7979        
					// Server reconnection configuration
					max-attempts = 3          
					delay-between-attempts = 1 #seconds
			...
					
##Requirements 


- Scala 2.10
- Akka 2.1.2
- SBT 12.2
- Idea Plugin 


##Useful commands for developers

- ``sbt one-jar`` : Taks to package the solution in one jar (including dependencies)
- ``sbt gen-idea``: Task to create idea project files

