package com.jteso.viagra.legacy;
/*************************************************************************
 *  Compilation:  javac EchoServer.java
 *  Execution:    java EchoServer port
 *  Dependencies: In.java [VERSION IN THIS DIRECTORY ONLY] Out.java
 *  
 *  Runs an echo server which listents for connections on port 4444,
 *  and echoes back whatever is sent to it.
 *
 *
 *  % java EchoServer 4444
 *
 *
 *  Limitations
 *  -----------
 *  The server is not multi-threaded, so at most one client can connect
 *  at a time.
 *
 *************************************************************************/

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class LegacyServer {

    public static void main(String[] args) throws Exception {

        // create socket
        int port = 7979;
        ServerSocket serverSocket = new ServerSocket(port);
        System.err.println("Started server on port " + port);

        // repeatedly wait for connections, and process
        while (true) {

            // a "blocking" call which waits until a connection is requested
            Socket clientSocket = serverSocket.accept();
            System.err.println("Accepted connection from client");

            // open up IO streams
         // Create the stream wrappers
			BufferedReader userInput = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			DataOutputStream userOutput = new DataOutputStream(clientSocket.getOutputStream());

            // waits for data and reads it in until connection dies
            // readLine() blocks until the server receives a new line from client
            String s;
            while ((s = userInput.readLine()) != null) {
                System.out.println("Server Received and echoing the message: " + s );
                userOutput.write(s.getBytes());
                
            }

            // close IO streams, then socket
            System.err.println("Closing connection with client");
            userOutput.close();
            userInput.close();
            clientSocket.close();
        }
    }
}
