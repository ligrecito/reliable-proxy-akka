package com.jteso.viagra.legacy

import java.net.InetSocketAddress

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorSystem
import akka.actor.IO
import akka.actor.IO.NewClient
import akka.actor.IO.Read
import akka.actor.IO.ServerHandle
import akka.actor.IOManager
import akka.actor.Props
import akka.util.ByteString


class TCPServer(port: Int) extends Actor with ActorLogging {
  var serverSocket: ServerHandle = _
  
  override def preStart {
    IOManager(context.system) listen new InetSocketAddress(port)
  }
  def receive ={
    case IO.Listening(server, address) => log.info("{} listening on address={}", self.path, address)
    
    case IO.Connected(socket, address) => println("successfully connected to" + address)
    
    case NewClient(server) => 
      server.accept()
      serverSocket = server
    case Read(socket, bytes) => {
      log.info("TCP Server is closing the socket")
      println("MCG received: " + ascii(bytes))
    }
  }
  
  private def ascii(bytes: ByteString): String = bytes.decodeString("US-ASCII").trim
}


object Mcg extends App{
  val port = 7979
  ActorSystem().actorOf(Props(new TCPServer(port)), "TCPServer")
}
