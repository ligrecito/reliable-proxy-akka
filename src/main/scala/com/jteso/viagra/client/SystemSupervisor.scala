package com.jteso.viagra.client

import akka.actor._
import akka.actor.SupervisorStrategy._

class SystemSupervisor(ClientConnectionProps: Props) extends Actor with ActorLogging{
  /* Supervision */
  override val supervisorStrategy = OneForOneStrategy(){
        case client: ClientConnLostException =>
          log.debug("are you here??")
          Restart
        case server: ServerNotAvailableException => Stop
          log.debug("stoping servernotavailable")
          Stop
  }

  /* Next ActorRef */
  val clientConnection = context.actorOf(ClientConnectionProps)
  context.watch(clientConnection)

  def receive = {
    case Terminated(actor) =>
      log.debug("System will be shutdown, due to actor: " + actor + " has stopped")
      context.system.shutdown
  }
}
