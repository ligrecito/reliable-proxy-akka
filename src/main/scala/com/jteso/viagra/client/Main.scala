package com.jteso.viagra.client

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory

object Main extends App{
  val system = ActorSystem("reliableProxySystem")

  /* Config  */
  val config = ConfigFactory.load
  val proxyPort  = config.getInt("akka.proxy.listening-tcp-port")
  val targetHost = config.getString("akka.proxy.target-host")
  val targetPort = config.getInt("akka.proxy.target-port")
  val maxAttempts = config.getInt("akka.proxy.max-attempts")
  val delayBetweenAttempts = config.getInt("akka.proxy.delay-between-attempts")

  printConfiguration()

  /* Actor Pipeline */
  val serverConnectionProps = Props(new ServerConnection(targetHost, targetPort))
  val serverConnectionSupervisorProps = Props(new ServerConnectionSupervisor(serverConnectionProps, maxAttempts, delayBetweenAttempts))
  val clientConnectionProps = Props(new ClientConnection(serverConnectionSupervisorProps, proxyPort))
  val systemSupervisor = system.actorOf(Props(new SystemSupervisor(clientConnectionProps)), "systemSupervisor")

  private def printConfiguration() {
    println("====================================")
    println("Listening port: " + proxyPort)
    println("Target host: " + targetHost)
    println("Target port: " + targetPort)
    println("Max attemps: " + maxAttempts)
    println("Max attemps: " + maxAttempts)
    println("====================================")

  }
}
