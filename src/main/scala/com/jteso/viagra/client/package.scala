package com.jteso.viagra

import akka.util.ByteString

package object client {
  
  object ReliableProxyProtocol {
    case class ServerResponse(bytes: ByteString)
    case class ClientRequest(bytes: ByteString)
    case class ServerConnectionReady()
    case class Reconnect()
  }
  
  /**
   * Exception when connection with client is being lost
   */
  @SerialVersionUID(1L)
  class ClientConnLostException(msg: String) extends Exception(msg) with Serializable
  
  /**
   * Exception when connection with server has been lost
   */
  @SerialVersionUID(1L)
  class ServerConnLostException(msg: String) extends Exception(msg) with Serializable
  
  /**
   * Exception when it was not possible to connect to server after N tries
   */
  @SerialVersionUID(1L)
  class ServerNotAvailableException(msg: String) extends Exception(msg) with Serializable


}