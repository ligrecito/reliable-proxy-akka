package com.jteso.viagra.client

import akka.actor._
import akka.actor.IO.SocketHandle
import akka.actor.SupervisorStrategy._
import akka.actor.IO.SocketHandle
import scala.Some

class ClientConnection(serverConnectionProps: Props,
                       listeningPort: Int) extends Actor with ActorLogging {
  import ReliableProxyProtocol._

  override val supervisorStrategy = AllForOneStrategy(){
    case _: ServerNotAvailableException => Escalate
  }

  var serverConnection = context.actorOf(serverConnectionProps)
  var currentClientSocket: Option[SocketHandle] = None
  
  def receive = {
    case IO.Listening(server, address) => 
      log.info("ReliableProxy listening on socket: " + address)
    
    case IO.NewClient(server) =>
      log.info("New incoming connection on server")
      closeCurrentConnection()
      currentClientSocket = Option(server.accept()) // register new connection

     case IO.Closed(server: IO.ServerHandle, cause) =>
       log.info("Server socket is closed, cause=" + cause)
       throw new ClientConnLostException("cause:" + cause)
        
     case IO.Read(socket, bytes) =>
       log.debug("Received request from client")
       serverConnection ! ClientRequest(bytes)
       
     case ServerResponse(bytes) =>
       log.debug("Received response from server")
       currentClientSocket match {
         case Some(s) =>
           s.write(bytes)
         case None =>
           log.warning("Message received from server while client connection has not been established yet")
       }
  }
  
  override def preStart = {
	  IOManager(context.system).listen("localhost", listeningPort)
  }
  
  // Restart involves: preRestart (postStop //by default kill all children) -->actorOf --> postRestart( preStart )
  override def preRestart(reason: Throwable, message: Option[Any]) = {
    log.debug("ClientConnection closing client sockets before restarting...")
    closeCurrentConnection()
  }

  private def closeCurrentConnection() {
    currentClientSocket foreach (_ close)
  }

}