package com.jteso.viagra.client

import scala.concurrent.duration.DurationInt
import akka.actor._
import akka.actor.SupervisorStrategy._
import akka.util.ByteString
import akka.actor.Terminated
import akka.actor.OneForOneStrategy

class ServerConnectionSupervisor(serverConnectionProps: Props,
                                 numberMaxOfAttempts: Int,
                                 delayInSeconds: Int) extends Actor with ActorLogging{
  import ReliableProxyProtocol._
  import context.dispatcher

  var connectionReady = false
  var requestBuffer: ByteString = ByteString.empty
  var remainingNumberOfAttempts = numberMaxOfAttempts
  
  override val supervisorStrategy = OneForOneStrategy(){
        case _: ServerConnLostException => Stop
  }
  
  /* Constructor */
  var serverConnection = context.actorOf(serverConnectionProps)
  context.watch(serverConnection) 
  
  def receive = {
    case ClientRequest(bytes) => 
        if (connectionReady){
        	log.debug("Forwarding msg to server:{} ...", bytes)
        	serverConnection forward ClientRequest(bytes)
        } else {
           log.debug("Buffering msg as server not available. System waiting to stop existing connection before going to failure mode")
           requestBuffer = requestBuffer ++ bytes
        }
    
    case ServerResponse(bytes) =>
       log.debug("Forwarding msg to client:{} ...", bytes)
        context.parent forward ServerResponse(bytes)
    
    case ServerConnectionReady =>
      remainingNumberOfAttempts = numberMaxOfAttempts
      connectionReady = true
      
    case Terminated(actor) => 
      log.info("ServerConnection has died. Trying to reconnect to target host and failure mode is activated.")
      connectionReady = false
      context.become(failureModeBehavior, discardOld=false)
      context.system.scheduler.scheduleOnce(delayInSeconds.seconds, self, Reconnect)

  }
  
  def failureModeBehavior(): Receive = {
     case ClientRequest(bytes) =>
        log.debug("Buffering message as Server connection not available yet: Msg = " + bytes)
        requestBuffer = requestBuffer ++ bytes

     case ServerResponse(bytes) =>
       log.debug("Forwarding msg to client:{} ...", bytes)
       context.parent forward ServerResponse(bytes)

     case ServerConnectionReady =>
       log.debug("Server connection established, exiting failure mode...")
       remainingNumberOfAttempts = numberMaxOfAttempts
       connectionReady = true
       flushBuffer
       context.unbecome() // set to normal behavior

     case Terminated(actor) => 
      log.info("ServerConnection has died. Trying to reconnect to target host and failure mode is activated.")
      connectionReady = false
      context.system.scheduler.scheduleOnce(delayInSeconds.seconds, self, Reconnect)

     case Reconnect =>
       remainingNumberOfAttempts = remainingNumberOfAttempts - 1
       if (remainingNumberOfAttempts <= 0)
         throw new ServerNotAvailableException("Impossible to connect to host after " + numberMaxOfAttempts + " failed attempts.")
       else{
         serverConnection = context.actorOf(serverConnectionProps)
         context.watch(serverConnection)
       }
  }
  
  private def flushBuffer() {
    if (requestBuffer.isEmpty == false) {
    	log.debug("Flushing buffer to server...")
	    serverConnection ! ClientRequest(requestBuffer)
	    requestBuffer = ByteString.empty
    }
  }
  
  override def preStart() {
    connectionReady = false
  }

}