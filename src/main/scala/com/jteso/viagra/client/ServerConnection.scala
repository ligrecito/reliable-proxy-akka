package com.jteso.viagra.client

import java.net.InetSocketAddress
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.IO
import akka.actor.IO.SocketHandle
import akka.actor.IOManager
import akka.actor.actorRef2Scala


class ServerConnection(targetHost: String, targetPort: Int) extends Actor with ActorLogging {
  import ReliableProxyProtocol._
  var supervisor = context.parent
  
  implicit val byteOrder = java.nio.ByteOrder.BIG_ENDIAN
  var currentClientSocket: Option[SocketHandle] = None
  
  def receive = {
    
    case IO.Connected(socket, address) =>
      log.info("Successfully connected to {}", address)
      currentClientSocket foreach (_ close) // drop existing connection
      currentClientSocket = Option(socket) // register new connection
      supervisor ! ServerConnectionReady
     
     case IO.Closed(socket: IO.SocketHandle, cause) =>
       log.info("Client socket is closed, cause={}", cause)
       throw new ServerConnLostException("cause:" + cause)
       
     case IO.Read(socket, bytes) =>
       log.debug("Received response from server")
       supervisor ! ServerResponse(bytes)
       
     case ClientRequest(bytes) =>
       log.debug("Sending request to server")
       currentClientSocket foreach (socket => socket.write(bytes))
  }
    
  
  override def preStart {
    log.info("Attempting to connect to server: " + targetHost + ":" + targetPort)
    IOManager(context.system).connect(new InetSocketAddress(targetHost, targetPort))
  }
  
  // Restart involves: preRestart (postStop //by default kill all children) -->actorOf --> postRestart( preStart )
  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    log.debug("ClientConnection closing client sockets before restarting...")
    currentClientSocket foreach (_ close)
  }

  

}  