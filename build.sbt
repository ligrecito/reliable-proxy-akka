name := "viagratunnel"
 
version := "1.0"
 
scalaVersion := "2.10.0"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")
 
resolvers ++= Seq(
  "akka" at "http://mvnrepository.com"
)

libraryDependencies ++= Seq(
  "com.typesafe.akka"   %%  "akka-actor"    % "2.1.0",
  "org.specs2"          %%  "specs2"        % "1.13" % "test"         
)

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

seq(Revolver.settings: _*)

